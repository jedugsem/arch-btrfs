#!/bin/sh
. ./vars
. ./utils.sh
ROOT="/mnt"
CRYPT_DEVICE="/dev/mapper/$CRYPT_NAME"

create_part

mount_part

pacstrap $ROOT $BASE

mkdir /mnt/home/installer
cp -r ./* /mnt/home/installer/
arch-chroot /mnt /bin/sh /home/installer/chroot-script.sh

#umount -R /mnt
#umount /mnt
